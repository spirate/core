<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Theme;


class CoreController extends Controller
{
    /**
     * @return Response|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        return Theme::view('welcome');
    }
}
